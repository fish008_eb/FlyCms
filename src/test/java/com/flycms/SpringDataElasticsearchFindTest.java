package com.flycms;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.IKAnalyzerUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.service.ILabelMergeService;
import com.flycms.modules.data.service.ILabelService;
import com.flycms.modules.data.service.IWeightService;
import com.flycms.modules.elastic.domain.dto.SearchDTO;
import com.flycms.modules.elastic.service.impl.ElasticSearchServiceImpl;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.notify.service.IAliyunService;
import com.flycms.modules.statistics.service.IStatisticsAccessPageService;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringDataElasticsearchFindTest {


    @Autowired
    private ElasticSearchServiceImpl elasticSearchServiceImpl;


    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IStatisticsAccessPageService statisticsAccessPageService;
    @Autowired
    private IWeightService weightService;

    @Autowired
    private IAliyunService aliyunService;

    @Autowired
    private ILabelService labelService;
    @Autowired
    private ILabelMergeService labelMergeService;
    /**
     * 查询全部
     */
    @Test
    // 创建索引
    public void createIndexStu() {
        statisticsAccessPageService.insertStatisticsAccessPage();
        weightService.updateTopicWeight();
    }

    @Test
    // 批量创建话题标签
    public void createTags() {
        List<GroupTopicDTO> topics = groupTopicService.selectGroupTopicAllList();
        topics.forEach(entity -> {
            try {
                labelMergeService.deleteLabelMerge(null,entity.getId(),1);
                List<String> list = IKAnalyzerUtils.cut(entity.getTitle(),true);
                for (String string : list) {
                    if (string != " " && string.length() >= 2) {
                        Label label = labelService.findLabelByTitle(string);
                        if (label==null) {
                            Label ss = new Label();
                            ss.setInfoId(entity.getId());
                            ss.setInfoType(1);
                            ss.setCountTopic(1);
                            ss.setTitle(string);
                            labelService.insertLabel(ss);
                        } else {
                            LabelMerge labelMerge=new LabelMerge();
                            labelMerge.setInfoType(1);
                            labelMerge.setInfoId(entity.getId());
                            labelMerge.setLabelId(label.getId());
                            labelMergeService.insertLabelMerge(labelMerge);
                            labelService.updateLabelByTopicCount(label.getId(),1);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Test
    public void setEmail() throws Exception {
        Pager<SearchDTO> kk= elasticSearchServiceImpl.relevantSearch(1, 10, "美", "530734146658500608","title", "content");
        kk.getList().forEach(entity -> {
            System.err.println(entity.getId()+"------------"+entity.getTitle());
                });

    }

    //获取中文的首字母
    @Test
    public void testPinyin() throws BadHanyuPinyinOutputFormatCombination {
        String name = "x互xx";
        char[] charArray = name.toCharArray();
        StringBuilder pinyin = new StringBuilder();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        // 设置大小写格式
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        // 设置声调格式：
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < charArray.length; i++) {
            //匹配中文,非中文转换会转换成null
            if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                if (hanyuPinyinStringArray != null) {
                    pinyin.append(hanyuPinyinStringArray[0].charAt(0));
                }
            }
        }
        System.err.println(pinyin);
    }

}
