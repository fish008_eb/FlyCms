package ${packageName}.service.impl;

#foreach ($column in $columns)
#if($column.javaField == 'createTime' || $column.javaField == 'updateTime')
import com.flycms.common.utils.DateUtils;
#break
#end
#end

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
#foreach ($column in $columns)
#if($column.javaField == 'userId')
import com.flycms.common.utils.SessionUtils;
#end
#end
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
#foreach ($column in $columns)
#if($column.javaField == 'adminId')
import com.flycms.framework.security.LoginAdmin;
import com.flycms.framework.security.service.TokenService;
import com.flycms.common.utils.ServletUtils;
#end
#end
import ${packageName}.mapper.${ClassName}Mapper;
#if($table.tree)
import ${packageName}.domain.${ClassName}TreeSelect;
#end
import ${packageName}.domain.${ClassName};
import ${packageName}.domain.dto.${ClassName}DTO;
import ${packageName}.service.I${ClassName}Service;

import java.util.ArrayList;
import java.util.List;

/**
 * ${functionName}Service业务层处理
 * 
 * @author ${author}
 * @date ${datetime}
 */
@Service
public class ${ClassName}ServiceImpl implements I${ClassName}Service 
{
    @Autowired
    private ${ClassName}Mapper ${className}Mapper;
#foreach ($column in $columns)
#if($column.javaField == 'adminId')
    @Autowired
    private TokenService tokenService;
#end
#end
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    @Override
    public int insert${ClassName}(${ClassName} ${className})
    {
#set($AttrName=$pkColumn.javaField.substring(0,1).toUpperCase() + ${pkColumn.javaField.substring(1)})
        ${className}.set${AttrName}(SnowFlakeUtils.nextId());
#foreach ($column in $columns)
#if($column.javaField == 'adminId')
        LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
        Long adminId = loginAdmin.getAdmin().getAdminId();
        ${className}.setAdminId(adminId);
#end
#if($column.javaField == 'userId')
        ${className}.setUserId(SessionUtils.getUser().getId());
#end
#if($column.javaField == 'createTime')
        ${className}.setCreateTime(DateUtils.getNowDate());
#end
#end
        return ${className}Mapper.insert${ClassName}(${className});
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
    @Override
    public int delete${ClassName}ByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s)
    {
        return ${className}Mapper.delete${ClassName}ByIds(${pkColumn.javaField}s);
    }

    /**
     * 删除${functionName}信息
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    @Override
    public int delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField})
    {
        return ${className}Mapper.delete${ClassName}ById(${pkColumn.javaField});
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    @Override
    public int update${ClassName}(${ClassName} ${className})
    {
#foreach ($column in $columns)
#if($column.javaField == 'updateTime')
        ${className}.setUpdateTime(DateUtils.getNowDate());
#end
#end
        return ${className}Mapper.update${ClassName}(${className});
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
#foreach($column in $columns)
#if($column.isRepeat)
    /**
     * 校验${column.columnComment}是否唯一
     *
     * @param ${pkColumn.javaField} ${pkColumn.columnComment}
     * @param ${column.javaField} ${column.columnComment}
     * @return 结果
     */
     @Override
     public String check${ClassName}$stringUtils.convertToCamelCase(${column.ColumnName})Unique(${pkColumn.javaType} ${pkColumn.javaField},${column.javaType} ${column.javaField})
     {
         ${ClassName} ${className} = new ${ClassName}();
         ${className}.set$stringUtils.convertToCamelCase(${pkColumn.javaField})(${pkColumn.javaField});
         ${className}.set$stringUtils.convertToCamelCase(${column.ColumnName})(${column.javaField});
         int count = ${className}Mapper.check${ClassName}$stringUtils.convertToCamelCase(${column.ColumnName})Unique(${className});
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }

#end
#end

    /**
     * 查询${functionName}
     * 
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    @Override
    public ${ClassName}DTO find${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField})
    {
        ${ClassName} ${className}=${className}Mapper.find${ClassName}ById(${pkColumn.javaField});
        return BeanConvertor.convertBean(${className},${ClassName}DTO.class);
    }


#if($table.crud)
    /**
     * 查询${functionName}列表
     *
     * @param ${className} ${functionName}
     * @return ${functionName}
     */
    @Override
    public Pager<${ClassName}DTO> select${ClassName}Pager(${ClassName} ${className}, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<${ClassName}DTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(${className});

        List<${ClassName}> ${className}List=${className}Mapper.select${ClassName}Pager(pager);
        List<${ClassName}DTO> dtolsit = new ArrayList<${ClassName}DTO>();
        ${className}List.forEach(entity -> {
            ${ClassName}DTO dto = new ${ClassName}DTO();
#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaField))
#if($column.javaField.length() > 2 && $column.javaField.substring(1,2).matches("[A-Z]"))
#set($AttrName=$column.javaField)
#else
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
#end
#if($column.list || $column.javaField == $pkColumn.javaField)
            dto.set${AttrName}(entity.get${AttrName}());
#end
#end
#end
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(${className}Mapper.query${ClassName}Total(pager));
        return pager;
    }
#elseif($table.tree)
    /**
    * 查询${functionName}所有列表
    *
    * @param ${className} ${functionName}
    * @return ${functionName}*/
    @Override
    public List<${ClassName}DTO> select${ClassName}List(${ClassName} column)
    {
        List<${ClassName}> ${className}= ${className}Mapper.select${ClassName}List(column);
        return BeanConvertor.copyList(${className},${ClassName}DTO.class);
    }

    /**
 * 构建前端所需要下拉树结构
 *
 * @param ${className} 部门列表
 * @return 下拉树结构列表
 */
    @Override
    public List<TreeSelect> build${ClassName}TreeSelect(List<${ClassName}DTO> ${className})
    {
        List<${ClassName}DTO> deptTrees = buildDeptTree(${className});
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 构建前端所需要树结构
     *
     * @param ${className} 部门列表
     * @return 树结构列表
     */
    @Override
    public List<${ClassName}DTO> build${ClassName}Tree(List<${ClassName}DTO> ${className})
    {
        List<${ClassName}DTO> returnList = new ArrayList<${ClassName}DTO>();
        List<Long> tempList = new ArrayList<Long>();
        for (${ClassName}DTO column : ${className})
        {
            tempList.add(column.getId());
        }
        for (Iterator<${ClassName}DTO> iterator = ${className}.iterator(); iterator.hasNext();)
        {
                ${ClassName}DTO column = (${ClassName}DTO) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId()))
            {
                recursionFn(${className}, column);
                returnList.add(column);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = ${className};
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<${ClassName}DTO> list, ${ClassName}DTO t)
    {
        // 得到子节点列表
        List<${ClassName}DTO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (${ClassName}DTO tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                // 判断是否有子节点
                Iterator<${ClassName}DTO> it = childList.iterator();
                while (it.hasNext())
                {
                        ${ClassName}DTO n = (${ClassName}DTO) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<${ClassName}DTO> getChildList(List<${ClassName}DTO> list, ${ClassName}DTO t)
    {
        List<${ClassName}DTO> tlist = new ArrayList<${ClassName}DTO>();
        Iterator<${ClassName}DTO> it = list.iterator();
        while (it.hasNext())
        {
                ${ClassName}DTO n = (${ClassName}DTO) it.next();
            if (StrUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<${ClassName}DTO> list, ${ClassName}DTO t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }
#end

    /**
     * 查询需要导出的${functionName}列表
     *
     * @param ${className} ${functionName}
     * @return ${functionName}集合
     */
    @Override
    public List<${ClassName}DTO> export${ClassName}List(${ClassName} ${className}) {
        return BeanConvertor.copyList(${className}Mapper.export${ClassName}List(${className}),${ClassName}DTO.class);
    }
}
