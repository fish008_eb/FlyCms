package com.flycms.common.utils.job;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import com.flycms.modules.monitor.domain.FlyJob;

/**
 * 定时任务处理（禁止并发执行）
 * 
 * @author kaifei sun
 *
 */
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, FlyJob flyJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(flyJob);
    }
}
