package com.flycms.common.utils.job;

import java.util.Date;

import com.flycms.common.utils.bean.BeanUtils;
import com.flycms.common.utils.spring.SpringUtils;
import com.flycms.common.constant.Constants;
import com.flycms.common.constant.ScheduleConstants;
import com.flycms.modules.monitor.service.IFlyJobLogService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.flycms.common.utils.ExceptionUtil;
import com.flycms.common.utils.StrUtils;
import com.flycms.modules.monitor.domain.FlyJob;
import com.flycms.modules.monitor.domain.FlyJobLog;

/**
 * 抽象quartz调用
 *
 * @author kaifei sun
 */
public abstract class AbstractQuartzJob implements Job
{
    private static final Logger log = LoggerFactory.getLogger(AbstractQuartzJob.class);

    /**
     * 线程本地变量
     */
    private static ThreadLocal<Date> threadLocal = new ThreadLocal<>();

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        FlyJob flyJob = new FlyJob();
        BeanUtils.copyBeanProp(flyJob, context.getMergedJobDataMap().get(ScheduleConstants.TASK_PROPERTIES));
        try
        {
            before(context, flyJob);
            if (flyJob != null)
            {
                doExecute(context, flyJob);
            }
            after(context, flyJob, null);
        }
        catch (Exception e)
        {
            log.error("任务执行异常  - ：", e);
            after(context, flyJob, e);
        }
    }

    /**
     * 执行前
     *
     * @param context 工作执行上下文对象
     * @param flyJob 系统计划任务
     */
    protected void before(JobExecutionContext context, FlyJob flyJob)
    {
        threadLocal.set(new Date());
    }

    /**
     * 执行后
     *
     * @param context 工作执行上下文对象
     * @param sysScheduleJob 系统计划任务
     */
    protected void after(JobExecutionContext context, FlyJob flyJob, Exception e)
    {
        Date startTime = threadLocal.get();
        threadLocal.remove();

        final FlyJobLog flyJobLog = new FlyJobLog();
        flyJobLog.setJobName(flyJob.getJobName());
        flyJobLog.setJobGroup(flyJob.getJobGroup());
        flyJobLog.setInvokeTarget(flyJob.getInvokeTarget());
        flyJobLog.setStartTime(startTime);
        flyJobLog.setStopTime(new Date());
        long runMs = flyJobLog.getStopTime().getTime() - flyJobLog.getStartTime().getTime();
        flyJobLog.setJobMessage(flyJobLog.getJobName() + " 总共耗时：" + runMs + "毫秒");
        if (e != null)
        {
            flyJobLog.setStatus(Constants.FAIL);
            String errorMsg = StrUtils.substring(ExceptionUtil.getExceptionMessage(e), 0, 2000);
            flyJobLog.setExceptionInfo(errorMsg);
        }
        else
        {
            flyJobLog.setStatus(Constants.SUCCESS);
        }

        // 写入数据库当中
        SpringUtils.getBean(IFlyJobLogService.class).addJobLog(flyJobLog);
    }

    /**
     * 执行方法，由子类重载
     *
     * @param context 工作执行上下文对象
     * @param flyJob 系统计划任务
     * @throws Exception 执行过程中的异常
     */
    protected abstract void doExecute(JobExecutionContext context, FlyJob flyJob) throws Exception;
}
