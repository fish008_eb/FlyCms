package com.flycms.common.utils.job;

import org.quartz.JobExecutionContext;
import com.flycms.modules.monitor.domain.FlyJob;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author kaifei sun
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob
{
    @Override
    protected void doExecute(JobExecutionContext context, FlyJob flyJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(flyJob);
    }
}
