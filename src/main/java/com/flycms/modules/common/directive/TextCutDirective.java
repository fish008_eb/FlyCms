package com.flycms.modules.common.directive;

import com.flycms.common.utils.StrUtils;
import com.flycms.common.web.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * 文本字符串截断
 * @author: kaifei sun
 * @date:   2020年11月17日 下午4:50:51
 */
@Component
public class TextCutDirective implements TemplateDirectiveModel {

	public static final String PARAM_S = "s";
	public static final String PARAM_LEN = "len";
	public static final String PARAM_APPEND = "append";

	@SuppressWarnings("unchecked")
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
						TemplateDirectiveBody body) throws TemplateException, IOException {
		String s = DirectiveUtils.getString(PARAM_S, params);
		Integer len = DirectiveUtils.getInt(PARAM_LEN, params);
		String append = DirectiveUtils.getString(PARAM_APPEND, params);
		if (s != null) {
			Writer out = env.getOut();
			if (len != null) {
				//字符串去除html，再取出字符串中空格
				s = StrUtils.replaceString(StrUtils.trimHtml2Txt(s));
				out.append(StrUtils.textCut(s, len, append));
			} else {
				out.append(s);
			}
		}
	}
}
