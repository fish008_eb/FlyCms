package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;
/**
 * 小组话题对象 fly_group_topic
 * 
 * @author admin
 * @date 2020-09-27
 */
@Data
public class GroupTopic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 话题ID */
    private Long id;
    /** 分类ID */
    private Long columnId;
    /** 小组ID */
    private Long groupId;
    /** 用户ID */
    private Long userId;
    /** 帖子标题 */
    private String title;
    /** 帖子标题图片 */
    private String titleImage;
    /** 帖子内容 */
    private String content;
    /** 回复统计 */
    private Integer countComment;
    /** 浏览数 */
    private Integer countView;
    /** 喜欢数 */
    private Integer countLove;
    //顶
    private Integer countDigg;
    //踩
    private Integer countBurys;
    //话题被关注数
    private Integer countFollow;
    //权重
    private Double weight;
    /** 是否置顶 */
    private String istop;
    /** 是否关闭 */
    private String isclose;
    /** 是否允许评论 */
    private String iscomment;
    /** 是否评论后显示内容 */
    private String iscommentshow;
    /** 是否精华帖子 */
    private String isposts;
    /** 审核 */
    private String isaudit;
    /** 删除 */
    private String deleted;
    /** 推荐 */
    private String[] recommends;
    /** 查看所需积分 */
    private Integer score;
    /** 审核状态 */
    private String status;
}
