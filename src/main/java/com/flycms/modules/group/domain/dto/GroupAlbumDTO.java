package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 小组帖子专辑数据传输对象 fly_group_album
 * 
 * @author admin
 * @date 2020-12-16
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupAlbumDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 专辑ID */
    @Excel(name = "专辑ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 专辑封面 */
    @Excel(name = "专辑封面")
    private String albumPic;
    /** 专辑名字 */
    @Excel(name = "专辑名字")
    private String albumName;
    /** 专辑介绍 */
    @Excel(name = "专辑介绍")
    private String albumDesc;
    /** 统计帖子 */
    @Excel(name = "统计帖子")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countTopic;
    /** 审核 */
    @Excel(name = "审核")
    private Integer isaudit;

}
