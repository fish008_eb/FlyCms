package com.flycms.modules.group.controller.manage;

import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupTopicColumn;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 帖子分类Controller
 * 
 * @author admin
 * @date 2020-11-03
 */
@RestController
@RequestMapping("/system/group/topicColumn")
public class GroupTopicColumnAdminController extends BaseController
{
    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增帖子分类
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:add')")
    @Log(title = "帖子分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupTopicColumn groupTopicColumn)
    {
        return toAjax(groupTopicColumnService.insertGroupTopicColumn(groupTopicColumn));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除帖子分类
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:remove')")
    @Log(title = "帖子分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupTopicColumnService.deleteGroupTopicColumnByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改帖子分类
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:edit')")
    @Log(title = "帖子分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupTopicColumn groupTopicColumn)
    {
        return toAjax(groupTopicColumnService.updateGroupTopicColumn(groupTopicColumn));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询帖子分类列表
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupTopicColumn groupTopicColumn,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupTopicColumnDTO> pager = groupTopicColumnService.selectGroupTopicColumnPager(groupTopicColumn, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出帖子分类列表
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:export')")
    @Log(title = "帖子分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupTopicColumn groupTopicColumn)
    {
        List<GroupTopicColumnDTO> groupTopicColumnList = groupTopicColumnService.selectGroupTopicColumnList(groupTopicColumn);
        ExcelUtil<GroupTopicColumnDTO> util = new ExcelUtil<GroupTopicColumnDTO>(GroupTopicColumnDTO.class);
        return util.exportExcel(groupTopicColumnList, "topicColumn");
    }

    /**
     * 获取帖子分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:topicColumn:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupTopicColumnService.findGroupTopicColumnById(id));
    }

}
