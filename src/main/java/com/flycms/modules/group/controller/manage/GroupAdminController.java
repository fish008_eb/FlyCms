package com.flycms.modules.group.controller.manage;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.dto.GroupDTO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 群组(小组)Controller
 * 
 * @author admin
 * @date 2020-09-25
 */
@RestController
@RequestMapping("/system/group/group")
public class GroupAdminController extends BaseController
{
    @Autowired
    private IGroupService groupService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增群组(小组)
     */
    @PreAuthorize("@ss.hasPermi('group:group:add')")
    @Log(title = "群组", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Group group)
    {
        if (UserConstants.NOT_UNIQUE.equals(groupService.checkGroupGroupNameUnique(group)))
        {
            return AjaxResult.error("新增群组(小组)'" + group.getGroupName() + "'失败，群组名称已存在");
        }
        return toAjax(groupService.insertGroup(group));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除群组(小组)
     */
    @PreAuthorize("@ss.hasPermi('group:group:remove')")
    @Log(title = "群组(小组)", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupService.deleteGroupByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组(小组)
     */
    @PreAuthorize("@ss.hasPermi('group:group:edit')")
    @Log(title = "群组(小组)", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Group group)
    {
        return toAjax(groupService.updateGroup(group));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询群组(小组)列表
     */
    @PreAuthorize("@ss.hasPermi('group:group:list')")
    @GetMapping("/list")
    public TableDataInfo list(Group group,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupDTO> pager = groupService.selectGroupPager(group, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出群组(小组)列表
     */
    @PreAuthorize("@ss.hasPermi('group:group:export')")
    @Log(title = "群组(小组)", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Group group,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupDTO> pager = groupService.selectGroupPager(group, pageNum, pageSize, sort, order);
        ExcelUtil<GroupDTO> util = new ExcelUtil<GroupDTO>(GroupDTO.class);
        return util.exportExcel(pager.getList(), "group");
    }

    /**
     * 获取群组(小组)详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:group:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupService.findGroupById(id));
    }
}
