package com.flycms.modules.group.service.impl;

import cn.hutool.dfa.WordTree;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.*;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.file.FileUploadUtils;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.service.ILabelMergeService;
import com.flycms.modules.data.service.ILabelService;
import com.flycms.modules.elastic.service.IElasticSearchService;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopicFollow;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.domain.vo.GroupTopicVO;
import com.flycms.modules.group.service.*;
import com.flycms.modules.images.service.IImagesService;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.service.ISiteService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserAccountService;
import com.flycms.modules.user.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupTopicMapper;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * 小组话题Service业务层处理
 * 
 * @author admin
 * @date 2020-09-27
 */
@CacheConfig(cacheNames = "groupTopic")
@Service
public class GroupTopicServiceImpl implements IGroupTopicService
{
    private static final Logger log = LoggerFactory.getLogger(GroupTopicServiceImpl.class);

    @Autowired
    private GroupTopicMapper groupTopicMapper;

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserAccountService userAccountService;

    @Autowired
    private IImagesService imagesService;

    @Autowired
    private ISiteService siteService;

    @Autowired
    private IGroupTopicFollowService groupTopicFollowService;

    @Autowired
    private IGroupUserService groupUserService;

    @Autowired
    private ILabelService labelService;

    @Autowired
    private ILabelMergeService labelMergeService;

    @Autowired
    private IElasticSearchService elasticSearchService;

    /////////////////////////////////
    ///////        增加       ////////
    /////////////////////////////////
    /**
     * 新增小组话题
     *
     * @param topicVo 接收页面小组话题内容
     * @return 结果
     */
    @Override
    @CachePut(value = "topic", key = "'topic_'+#topicVo.id", unless = "#result eq null")
    @Transactional
    public AjaxResult insertGroupTopic(GroupTopicVO topicVo)
    {
        //上传文件不为空则上传文件
        if(topicVo.getFile()!=null){
            if(!topicVo.getFile().isEmpty()){
                String fileName = topicVo.getFile().getOriginalFilename();
                String filePath = FileUploadUtils.getImgPath();
                File dest = new File(filePath + fileName);
                try {
                    topicVo.getFile().transferTo(dest);
                    //log.info("上传成功");
                } catch (IOException e) {
                    log.error(e.toString(), e);
                }
            }
        }

        Group group= groupService.findGroupById(Long.valueOf(topicVo.getGroupId()));
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        if("0".equals(group.getIspost())){
            return AjaxResult.error("本小组暂不允许发帖！");
        }
        GroupUser findgroupUser = new GroupUser();
        findgroupUser.setGroupId(group.getId());
        findgroupUser.setUserId(SessionUtils.getUser().getId());
        GroupUser userDTO=groupUserService.findGroupUser(findgroupUser);
        if(userDTO == null || userDTO.getStatus() == 0){
            return AjaxResult.error("您未关注本小组或者未通过审核");
        }

        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setId(SnowFlakeUtils.nextId());
        //用户id
        groupTopic.setUserId(SessionUtils.getUser().getId());
        groupTopic.setCreateTime(DateUtils.getNowDate());
        String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),topicVo.getContent());
        groupTopic.setContent(content);
        groupTopic.setGroupId(Long.valueOf(topicVo.getGroupId()));
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if("1".equals(group.getIspostaudit())){
            groupTopic.setIsaudit("0");
        }else{
            groupTopic.setIsaudit("1");
        }
        Site site=siteService.selectSite(478279584488632368l);
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getPostVerify() ==1){
                groupTopic.setStatus("0");
            }else{
                groupTopic.setStatus("1");
            }
        }

        int count = groupTopicMapper.insertGroupTopic(groupTopic);
        if(count > 0) {
            //添加用户关注话题关联
            GroupTopicFollow groupTopicFollow = new GroupTopicFollow();
            groupTopicFollow.setTopicId(groupTopic.getId());
            groupTopicFollow.setUserId(groupTopic.getUserId());
            groupTopicFollowService.insertGroupTopicFollow(groupTopicFollow);
            if (!StringUtils.isBlank(topicVo.getLabelList())) {
                String[] tags = topicVo.getLabelList().split(","); //转换为数组
                if (tags.length > 5) {
                    return AjaxResult.error("话题数不能大于5个");
                }
                for (String string : tags) {
                    if (string != "" && string.length() >= 2) {
                        Label label = labelService.findLabelByTitle(string);
                        if (label == null) {
                            Label ss = new Label();
                            ss.setInfoId(groupTopic.getId());
                            ss.setInfoType(1);
                            ss.setCountTopic(1);
                            ss.setTitle(string);
                            labelService.insertLabel(ss);
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setLabelId(label.getId());
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setInfoType(1);
                            if (!labelMergeService.checkLabelMergeUnique(labelMerge)) {
                                labelMergeService.insertLabelMerge(labelMerge);
                            }
                        } else {
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setInfoType(1);
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setLabelId(label.getId());
                            labelMergeService.insertLabelMerge(labelMerge);
                            labelService.updateLabelByTopicCount(label.getId(), 1);
                        }
                    }
                }
            } else {
                List<String> list = IKAnalyzerUtils.cut(topicVo.getTitle(), true);
                for (String string : list) {
                    if (string != " " && string.length() >= 2) {
                        Label label = labelService.findLabelByTitle(string);
                        if (label == null) {
                            Label ss = new Label();
                            ss.setInfoId(groupTopic.getId());
                            ss.setInfoType(1);
                            ss.setCountTopic(1);
                            ss.setTitle(string);
                            labelService.insertLabel(ss);
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setLabelId(ss.getId());
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setInfoType(1);
                            if (!labelMergeService.checkLabelMergeUnique(labelMerge)) {
                                labelMergeService.insertLabelMerge(labelMerge);
                            }
                        } else {
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setInfoType(1);
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setLabelId(label.getId());
                            labelMergeService.insertLabelMerge(labelMerge);
                            labelService.updateLabelByTopicCount(label.getId(), 1);
                        }
                    }
                }
            }

            if ("1".equals(group.getIspostaudit())) {
                return AjaxResult.success("本栏内容需要审核，请耐心等待！", groupTopic.getId().toString());
            }
            userAccountService.updateUserTopic(groupTopic.getUserId());
            groupService.updateCountTopic(groupTopic.getGroupId());

            Map<String, Object> map = new HashMap<>();
            map.put("infoType", "1");
            map.put("groupId", String.valueOf(groupTopic.getGroupId()));
            map.put("columnId", String.valueOf(groupTopic.getColumnId()));
            map.put("userId", String.valueOf(groupTopic.getUserId()));
            map.put("title", groupTopic.getTitle());
            map.put("content", StrUtils.trimHtml2Txt(groupTopic.getContent()));
            String timeZoneConvert = DateUtils.timeZoneConvert(groupTopic.getCreateTime().getTime(), "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "Asia/Shanghai");
            map.put("createTime", timeZoneConvert);
            elasticSearchService.createDocument("topic", groupTopic.getId().toString(), map);
            return AjaxResult.success("发布成功", groupTopic.getId().toString());
        }
        return AjaxResult.error("未知错误！");
    }

    /**
     * 采集器发布接口新增小组话题
     *
     * @param groupTopic 接收页面小组话题内容
     * @return 结果
     */
    @Override
    @CachePut(value = "topic", key = "'topic_'+#groupTopic.id", unless = "#result eq null")
    @Transactional
    public int insertApiGroupTopic(GroupTopic groupTopic)
    {
        groupTopic.setId(SnowFlakeUtils.nextId());
        groupTopic.setCreateTime(DateUtils.getNowDate());
        String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),groupTopic.getContent());
        groupTopic.setContent(content);
        return groupTopicMapper.insertGroupTopic(groupTopic);
    }

    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组话题
     *
     * @param ids 需要删除的小组话题ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicByIds(Long[] ids)
    {
        return groupTopicMapper.deleteGroupTopicByIds(ids);
    }

    /**
     * 删除小组话题信息
     *
     * @param id 小组话题ID
     * @return 结果
     */
    @Override
    @CacheEvict(value = "topic", key = "'topic_'+#id")
    public int deleteGroupTopicById(Long id)
    {
        return groupTopicMapper.deleteGroupTopicById(id);
    }


    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组话题
     *
     * @param topicVo 小组话题
     * @return 结果
     */
    @Override
    @Transactional
    @CacheEvict(value = "topic", key = "'topic_'+#topicVo.id")
    public AjaxResult updateGroupTopic(GroupTopicVO topicVo)
    {
        Group group= groupService.findGroupById(Long.valueOf(topicVo.getGroupId()));
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setId(Long.valueOf(topicVo.getId()));
        groupTopic.setGroupId(Long.valueOf(topicVo.getGroupId()));
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setTitleImage(topicVo.getTitleImage());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setUserId(SessionUtils.getUser().getId());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setUpdateTime(DateUtils.getNowDate());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if("1".equals(group.getIspostaudit())){
            groupTopic.setIsaudit("0");
        }else{
            groupTopic.setIsaudit("1");
        }
        Site site=siteService.selectSite(478279584488632368l);
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getPostVerify() ==1){
                groupTopic.setStatus("0");
            }else{
                groupTopic.setStatus("1");
            }
        }
        groupTopic.setUpdateTime(DateUtils.getNowDate());
        if(groupTopic.getContent() != null){
            String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),groupTopic.getContent());
            groupTopic.setContent(content);
        }
        int count = groupTopicMapper.updateGroupTopic(groupTopic);
        if(count >0) {
            labelMergeService.deleteLabelMerge(null, groupTopic.getId(), 1);
            if (!StringUtils.isBlank(topicVo.getLabelList())) {
                String[] tags = topicVo.getLabelList().split(","); //转换为数组
                if (tags.length > 5) {
                    return AjaxResult.error("话题数不能大于5个");
                }
                for (String string : tags) {
                    if (string != "" && string.length() >= 2) {
                        Label label = labelService.findLabelByTitle(string);
                        if (label == null) {
                            Label ss = new Label();
                            ss.setInfoId(groupTopic.getId());
                            ss.setInfoType(1);
                            ss.setCountTopic(1);
                            ss.setTitle(string);
                            labelService.insertLabel(ss);
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setLabelId(label.getId());
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setInfoType(1);
                            if (!labelMergeService.checkLabelMergeUnique(labelMerge)) {
                                labelMergeService.insertLabelMerge(labelMerge);
                            }
                        } else {
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setInfoType(1);
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setLabelId(label.getId());
                            labelMergeService.insertLabelMerge(labelMerge);
                            labelService.updateLabelByTopicCount(label.getId(), 1);
                        }
                    }
                }
            } else {
                List<String> list = IKAnalyzerUtils.cut(topicVo.getTitle(), true);
                for (String string : list) {
                    if (string != " " && string.length() >= 2) {
                        Label label = labelService.findLabelByTitle(string);
                        if (label == null) {
                            Label ss = new Label();
                            ss.setInfoId(groupTopic.getId());
                            ss.setInfoType(1);
                            ss.setCountTopic(1);
                            ss.setTitle(string);
                            labelService.insertLabel(ss);
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setLabelId(label.getId());
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setInfoType(1);
                            if (!labelMergeService.checkLabelMergeUnique(labelMerge)) {
                                labelMergeService.insertLabelMerge(labelMerge);
                            }
                        } else {
                            LabelMerge labelMerge = new LabelMerge();
                            labelMerge.setInfoType(1);
                            labelMerge.setInfoId(groupTopic.getId());
                            labelMerge.setLabelId(label.getId());
                            labelMergeService.insertLabelMerge(labelMerge);
                            labelService.updateLabelByTopicCount(label.getId(), 1);
                        }
                    }
                }
            }

            if ("1".equals(group.getIspostaudit())) {
                return AjaxResult.success("本栏内容需要审核，请耐心等待！", groupTopic.getId().toString());
            }
            userAccountService.updateUserTopic(SessionUtils.getUser().getId());
            groupService.updateCountTopic(groupTopic.getGroupId());
            Map<String, Object> map = new HashMap<>();
            map.put("infoType", "1");
            map.put("groupId", String.valueOf(groupTopic.getGroupId()));
            map.put("columnId", String.valueOf(groupTopic.getColumnId()));
            map.put("userId", String.valueOf(groupTopic.getUserId()));
            map.put("title", groupTopic.getTitle());
            map.put("content", StrUtils.trimHtml2Txt(groupTopic.getContent()));
            String timeZoneConvert = DateUtils.timeZoneConvert(topicVo.getCreateTime().getTime(), "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "Asia/Shanghai");
            map.put("createTime", timeZoneConvert);
            elasticSearchService.updateDocument("topic", groupTopic.getId().toString(), map);
            return AjaxResult.success("修改成功","539623310711848960");
        }

        return AjaxResult.error("未知错误！");
    }

    /**
     * 修改小组话题假删除
     *
     * @param id 小组话题ID
     * @return 结果
     */
    @Override
    public int updateDeleteGroupTopicById(Long id){
        return groupTopicMapper.updateDeleteGroupTopicById(id);
    }

    /**
     * 更新话题评论数量
     *
     * @param id
     * @return
     */
    @Override
    public int updateCountComment(Long id)
    {
        return groupTopicMapper.updateCountComment(id);
    }

    /**
     * 更新用户话题数量
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public int updateUserCountTopic(Long userId){
        return groupTopicMapper.updateUserCountTopic(userId);
    }

    /**
     * 更新话题被关注数量
     *
     * @param id 话题ID
     * @return
     */
    @Override
    public int updateTopicFollowCount(Long id){
        return groupTopicMapper.updateTopicFollowCount(id);
    }

    /**
     * 更新话题浏览数量
     *
     * @param id 小组话题ID
     * @return
     */
    @Override
    public int updateCountView(Long id){
        return groupTopicMapper.updateCountView(id);
    }

    /**
     * 更新话题排序权重
     *
     * @param weight 权重值
     * @param id  小组话题ID
     * @return
     */
    @Override
    public int updateWeight(Double weight,Long id){
        return groupTopicMapper.updateWeight(weight,id);
    }
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组小组话题是否唯一
     *
     * @param topicVo 小组话题
     * @return 结果
     */
    @Override
    public String checkGroupTopicNameUnique(GroupTopicVO topicVo)
    {
        GroupTopic topic=new GroupTopic();
        topic.setUserId(Long.valueOf(SessionUtils.getUser().getId()));
        if(topicVo.getGroupId() != null ){
            topic.setGroupId(Long.valueOf(topicVo.getGroupId()));
        }
        topic.setTitle(topicVo.getTitle());
        int count = groupTopicMapper.checkGroupTopicNameUnique(topic);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 查询小组话题
     * 
     * @param id 小组话题ID
     * @return 小组话题
     */
    @Override
    @Cacheable(value = "topic",key = "'topic_'+#id",sync=true)
    public GroupTopicDTO findGroupTopicById(Long id)
    {
        GroupTopic groupTopic=groupTopicMapper.findGroupTopicById(id);
        if(groupTopic != null){
            //替换敏感词
            String sensitiveWord = siteService.selectSite(478279584488632368l).getSensitiveWord();
            if(sensitiveWord != null){
                List<String> result = Arrays.asList(sensitiveWord.split(","));
                WordTree tree = new WordTree();
                for(String key : result){
                    key = key.trim();
                    tree.addWord(key);
                }
                List<String> matchAll = tree.matchAll(groupTopic.getTitle()+groupTopic.getContent(), -1, true, true);
                groupTopic.setContent(SensitiveWordUtils.replaceSensitiveWord(groupTopic.getContent(),matchAll));
                String title = groupTopic.getTitle();
                groupTopic.setTitle(SensitiveWordUtils.replaceSensitiveWord(title,matchAll));
            }
            return BeanConvertor.convertBean(groupTopic,GroupTopicDTO.class);
        }
        return null;
    }

    /**
     * 按ID查询内容的上一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    @Override
    public GroupTopicDTO findGroupTopicByPrePage(GroupTopic groupTopic)
    {
        GroupTopic topic=groupTopicMapper.findGroupTopicByPrePage(groupTopic);
        return BeanConvertor.convertBean(topic,GroupTopicDTO.class);
    }

    /**
     * 按ID查询内容的下一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    @Override
    public GroupTopicDTO findGroupTopicByNextPage(GroupTopic groupTopic)
    {
        GroupTopic topic=groupTopicMapper.findGroupTopicByNextPage(groupTopic);
        return BeanConvertor.convertBean(topic,GroupTopicDTO.class);
    }

    /**
     * 查询小组话题数量
     *
     * @param groupTopic 小组话题
     * @return 小组话题数量
     */
    @Override
    public int queryGroupTopicTotal(GroupTopic groupTopic) {
        Pager pager=new Pager();
        pager.setEntity(groupTopic);
        return groupTopicMapper.queryGroupTopicTotal(pager);
    }


    /**
     * 查询小组话题列表
     *
     * @param groupTopic 小组话题
     * @return 小组话题
     */
    @Override
    public Pager<GroupTopicDTO> selectGroupTopicPager(GroupTopic groupTopic, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupTopicDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopic);

        List<GroupTopic> groupTopicList=groupTopicMapper.selectGroupTopicPager(pager);
        List<GroupTopicDTO> dtolsit = new ArrayList<GroupTopicDTO>();
        groupTopicList.forEach(entity -> {
            GroupTopicDTO dto = new GroupTopicDTO();
            dto.setId(entity.getId());
            dto.setColumnId(entity.getColumnId());
            GroupTopicColumnDTO column=groupTopicColumnService.findGroupTopicColumnById(entity.getColumnId());
            if(column != null){
                dto.setColumnName(column.getColumnName());
            }
            Group group=groupService.findGroupById(entity.getGroupId());
            if(group != null){
                dto.setGroupName(group.getGroupName());
            }
            dto.setGroupId(entity.getGroupId());
            dto.setUserId(entity.getUserId());
            User user=userService.findUserById(entity.getUserId());
            if(user != null){
                dto.setNickname(user.getNickname());
            }
            dto.setTitle(entity.getTitle());
            dto.setTitleImage(entity.getTitleImage());
            dto.setContent(entity.getContent());
            dto.setCountComment(entity.getCountComment());
            dto.setCountView(entity.getCountView());
            dto.setCountDigg(entity.getCountDigg());
            dto.setCountBurys(entity.getCountBurys());
            dto.setIstop(entity.getIstop());
            dto.setIsclose(entity.getIsclose());
            dto.setIscomment(entity.getIscomment());
            dto.setIscommentshow(entity.getIscommentshow());
            dto.setIsposts(entity.getIsposts());
            dto.setIsaudit(entity.getIsaudit());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicMapper.queryGroupTopicTotal(pager));
        return pager;
    }

    /**
     * 查询小组话题所有列表
     *
     * @param total 查询数量
     * @return
     */
    @Override
    public List<GroupTopic> selectRandTopicList(int total){
        return groupTopicMapper.selectRandTopicList(total);
    }

    /**
     * 查询需要导出的小组话题列表
     *
     * @return 所有小组话题列表
     */
    @Override
    public List<GroupTopicDTO> selectGroupTopicAllList() {
        return BeanConvertor.copyList(groupTopicMapper.selectGroupTopicAllList(),GroupTopicDTO.class);
    }

}
