package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.domain.vo.GroupTopicVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 小组话题Service接口
 * 
 * @author admin
 * @date 2020-09-27
 */
public interface IGroupTopicService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组话题
     *
     * @param topicVo 接收页面小组话题内容
     * @return 结果
     */
    public AjaxResult insertGroupTopic(GroupTopicVO topicVo);

    /**
     * 采集器发布接口新增小组话题
     *
     * @param groupTopic 接收页面小组话题内容
     * @return 结果
     */
    public int insertApiGroupTopic(GroupTopic groupTopic);
    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组话题
     *
     * @param ids 需要删除的小组话题ID
     * @return 结果
     */
    public int deleteGroupTopicByIds(Long[] ids);

    /**
     * 删除小组话题信息
     *
     * @param id 小组话题ID
     * @return 结果
     */
    public int deleteGroupTopicById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////


    /**
     * 修改小组话题
     *
     * @param topicVo 小组话题
     * @return 结果
     */
    public AjaxResult updateGroupTopic(GroupTopicVO topicVo);

    /**
     * 修改小组话题假删除
     *
     * @param id 小组话题ID
     * @return 结果
     */
    public int updateDeleteGroupTopicById(Long id);
    /**
     * 更新话题评论数量
     *
     * @param id
     * @return
     */
    public int updateCountComment(Long id);

    /**
     * 更新用户话题数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserCountTopic(Long userId);

    /**
     * 更新话题被关注数量
     *
     * @param id 话题ID
     * @return
     */
    public int updateTopicFollowCount(Long id);

    /**
     * 更新话题浏览数量
     *
     * @param id 小组话题ID
     * @return
     */
    public int updateCountView(Long id);

    /**
     * 更新话题排序权重
     *
     * @param weight 权重值
     * @param id  小组话题ID
     * @return
     */
    public int updateWeight(Double weight,Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组小组话题是否唯一
     *
     * @param topicVo 小组话题
     * @return 结果
     */
    public String checkGroupTopicNameUnique(GroupTopicVO topicVo);

    /**
     * 查询小组话题
     * 
     * @param id 小组话题ID
     * @return 小组话题
     */
    public GroupTopicDTO findGroupTopicById(Long id);

    /**
     * 按ID查询内容的上一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    public GroupTopicDTO findGroupTopicByPrePage(GroupTopic groupTopic);

    /**
     * 按ID查询内容的下一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    public GroupTopicDTO findGroupTopicByNextPage(GroupTopic groupTopic);

    /**
     * 查询小组话题数量
     *
     * @param groupTopic 小组话题
     * @return 小组话题数量
     */
    public int queryGroupTopicTotal(GroupTopic groupTopic);

    /**
     * 查询小组话题列表
     * 
     * @param groupTopic 小组话题
     * @return 小组话题集合
     */
    public Pager<GroupTopicDTO> selectGroupTopicPager(GroupTopic groupTopic, Integer page, Integer limit, String sort, String order);

    /**
     * 查询小组话题所有列表
     *
     * @param total 查询数量
     * @return
     */
    public List<GroupTopic> selectRandTopicList(int total);

    /**
     * 查询需要导出的小组话题列表
     *
     * @return 所有小组话题列表
     */
    public List<GroupTopicDTO> selectGroupTopicAllList();
}
