package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicColumn;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;

import java.util.List;

/**
 * 帖子分类Service接口
 * 
 * @author admin
 * @date 2020-11-03
 */
public interface IGroupTopicColumnService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    public int insertGroupTopicColumn(GroupTopicColumn groupTopicColumn);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除帖子分类
     *
     * @param ids 需要删除的帖子分类ID
     * @return 结果
     */
    public int deleteGroupTopicColumnByIds(Long[] ids);

    /**
     * 删除帖子分类信息
     *
     * @param id 帖子分类ID
     * @return 结果
     */
    public int deleteGroupTopicColumnById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    public int updateGroupTopicColumn(GroupTopicColumn groupTopicColumn);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询帖子分类数量
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类数量
     */
    public String checkGroupTopicColumnUnique(GroupTopicColumn groupTopicColumn);

    /**
     * 按shortUrl查询群组(小组)分类信息
     *
     * @param shortUrl 短域名地址
     * @return 群组(小组)分类
     */
    public GroupTopicColumn findGroupTopicColumnByShorturl(String shortUrl);

    /**
     * 查询帖子分类
     * 
     * @param id 帖子分类ID
     * @return 帖子分类
     */
    public GroupTopicColumnDTO findGroupTopicColumnById(Long id);

    /**
     * 查询帖子分类列表
     * 
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类集合
     */
    public Pager<GroupTopicColumnDTO> selectGroupTopicColumnPager(GroupTopicColumn groupTopicColumn, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的帖子分类列表
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类集合
     */
    public List<GroupTopicColumnDTO> selectGroupTopicColumnList(GroupTopicColumn groupTopicColumn);
}
