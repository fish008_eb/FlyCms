package com.flycms.modules.group.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.images.service.IImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupTopicCommentMapper;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;
import com.flycms.modules.group.service.IGroupTopicCommentService;

import java.util.ArrayList;
import java.util.List;

/**
 * 话题回复/评论Service业务层处理
 * 
 * @author admin
 * @date 2020-12-15
 */
@Service
public class GroupTopicCommentServiceImpl implements IGroupTopicCommentService
{
    @Autowired
    private IImagesService imagesService;

    @Autowired
    private GroupTopicCommentMapper groupTopicCommentMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    @Override
    public int insertGroupTopicComment(GroupTopicComment groupTopicComment)
    {
        groupTopicComment.setId(SnowFlakeUtils.nextId());
        //过来内容中的html标签
        groupTopicComment.setContent(StrUtils.getTextFromHtml(groupTopicComment.getContent()));
        if(groupTopicComment.getReferId() > 0){
            int referIdCount=groupTopicCommentMapper.queryReCommentCount(groupTopicComment.getReferId());
            groupTopicCommentMapper.updateCountComment(referIdCount+1,groupTopicComment.getReferId());
        }
        int storey = groupTopicCommentMapper.queryTopicCommentCount(groupTopicComment.getTopicId());
        String content=imagesService.replaceContent(0,groupTopicComment.getId(),groupTopicComment.getUserId(),groupTopicComment.getContent());
        groupTopicComment.setContent(content);
        groupTopicComment.setStorey(storey+1);
        groupTopicComment.setCreateTime(DateUtils.getNowDate());
        return groupTopicCommentMapper.insertGroupTopicComment(groupTopicComment);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的话题回复/评论ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicCommentByIds(Long[] ids)
    {
        return groupTopicCommentMapper.deleteGroupTopicCommentByIds(ids);
    }

    /**
     * 删除话题回复/评论信息
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicCommentById(Long id)
    {
        return groupTopicCommentMapper.deleteGroupTopicCommentById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    @Override
    public int updateGroupTopicComment(GroupTopicComment groupTopicComment)
    {
        return groupTopicCommentMapper.updateGroupTopicComment(groupTopicComment);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验话题回复/评论是否唯一
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    @Override
    public String checkGroupTopicCommentUnique(GroupTopicComment groupTopicComment)
    {
        int count = groupTopicCommentMapper.checkGroupTopicCommentUnique(groupTopicComment);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 按话题ID查询最新话题回复信息
     *
     * @param topicId 话题ID
     * @return
     */
    @Override
    public GroupTopicComment findNewestGroupTopicComment(Long topicId){
        return groupTopicCommentMapper.findNewestGroupTopicComment(topicId);
    }

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    @Override
    public GroupTopicCommentDTO findGroupTopicCommentById(Long id)
    {
        GroupTopicComment groupTopicComment = groupTopicCommentMapper.findGroupTopicCommentById(id);
        return BeanConvertor.convertBean(groupTopicComment, GroupTopicCommentDTO.class);
    }


    /**
     * 查询话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论
     */
    @Override
    public Pager<GroupTopicCommentDTO> selectGroupTopicCommentPager(GroupTopicComment groupTopicComment, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupTopicCommentDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", false,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopicComment);

        List<GroupTopicComment> groupTopicCommentList = groupTopicCommentMapper.selectGroupTopicCommentPager(pager);
        List<GroupTopicCommentDTO> dtolsit = new ArrayList<GroupTopicCommentDTO>();
        groupTopicCommentList.forEach(entity -> {
            GroupTopicCommentDTO dto = new GroupTopicCommentDTO();
            dto.setId(entity.getId());
            dto.setReferId(entity.getReferId());
            dto.setTopicId(entity.getTopicId());
            dto.setUserId(entity.getUserId());
            dto.setContent(entity.getContent());
            dto.setCountComment(entity.getCountComment());
            dto.setCountDigg(entity.getCountDigg());
            dto.setCountBurys(entity.getCountBurys());
            dto.setStorey(entity.getStorey());
            dto.setIspublic(entity.getIspublic());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicCommentMapper.queryGroupTopicCommentTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    @Override
    public List<GroupTopicCommentDTO> exportGroupTopicCommentList(GroupTopicComment groupTopicComment) {
        return BeanConvertor.copyList(groupTopicCommentMapper.exportGroupTopicCommentList(groupTopicComment), GroupTopicCommentDTO.class);
    }
}
