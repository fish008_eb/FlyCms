package com.flycms.modules.group.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;

import java.util.List;

/**
 * 话题回复/评论Service接口
 * 
 * @author admin
 * @date 2020-12-15
 */
public interface IGroupTopicCommentService
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int insertGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的话题回复/评论ID
     * @return 结果
     */
    public int deleteGroupTopicCommentByIds(Long[] ids);

    /**
     * 删除话题回复/评论信息
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    public int deleteGroupTopicCommentById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int updateGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验话题回复/评论是否唯一
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public String checkGroupTopicCommentUnique(GroupTopicComment groupTopicComment);

    /**
     * 按话题ID查询最新话题回复信息
     *
     * @param topicId 话题ID
     * @return
     */
    public GroupTopicComment findNewestGroupTopicComment(Long topicId);

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    public GroupTopicCommentDTO findGroupTopicCommentById(Long id);

    /**
     * 查询话题回复/评论列表
     * 
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public Pager<GroupTopicCommentDTO> selectGroupTopicCommentPager(GroupTopicComment groupTopicComment, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public List<GroupTopicCommentDTO> exportGroupTopicCommentList(GroupTopicComment groupTopicComment);
}
