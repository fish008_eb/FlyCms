package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.GroupTopicRecommend;
import org.springframework.stereotype.Repository;

/**
 * 内容推荐Mapper接口
 * 
 * @author admin
 * @date 2020-12-07
 */
@Repository
public interface GroupTopicRecommendMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    public int insertGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////

    /**
     * 按内容类型和内容id删除推荐设置
     *
     * @param contentTypeId
     * @param contentId
     * @return
     */
    public int deleteGroupTopicRecommendByContentId(int contentTypeId,Long contentId);

    /**
     * 删除内容推荐
     *
     * @param id 内容推荐ID
     * @return 结果
     */
    public int deleteGroupTopicRecommendById(Long id);

    /**
     * 批量删除内容推荐
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTopicRecommendByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    public int updateGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验内容推荐关联是否唯一
     *
     * @param groupTopicRecommend 小组话题
     * @return 结果
     */
    public int checkGroupTopicRecommendUnique(GroupTopicRecommend groupTopicRecommend);

    /**
     * 查询内容推荐
     *
     * @param contentTypeId 内容类型
     * @param groupId  小组id
     * @param contentId 内容推荐ID
     * @return
     */
    public List<String> findGroupTopicRecommendById(Long contentTypeId,Long groupId,Long contentId);

    /**
     * 查询内容推荐数量
     *
     * @param pager 分页处理类
     * @return 内容推荐数量
     */
    public int queryGroupTopicRecommendTotal(Pager pager);

    /**
     * 查询内容推荐列表
     * 
     * @param pager 分页处理类
     * @return 内容推荐集合
     */
    public List<GroupTopic> selectGroupTopicRecommendPager(Pager pager);

    /**
     * 查询需要导出的内容推荐列表
     *
     * @param groupTopicRecommend 内容推荐
     * @return 内容推荐集合
     */
    public List<GroupTopicRecommend> exportGroupTopicRecommendList(GroupTopicRecommend groupTopicRecommend);
}
