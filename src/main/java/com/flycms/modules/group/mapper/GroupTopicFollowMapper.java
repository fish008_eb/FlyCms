package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopicFollow;
import org.springframework.stereotype.Repository;

/**
 * 话题关注Mapper接口
 * 
 * @author admin
 * @date 2021-02-01
 */
@Repository
public interface GroupTopicFollowMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    public int insertGroupTopicFollow(GroupTopicFollow groupTopicFollow);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题关注
     *
     * @param id 话题关注ID
     * @return 结果
     */
    public int deleteGroupTopicFollowById(Long id);

    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTopicFollowByIds(Long[] ids);

    /**
     * 按用户id和话题id查询是否关注
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    public int deleteTopicUserFollow(Long topicId,Long userId);
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    public int updateGroupTopicFollow(GroupTopicFollow groupTopicFollow);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 校验话题关注是否唯一
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    public int checkTopicFollowUnique(Long topicId,Long userId);

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    public GroupTopicFollow findGroupTopicFollowById(Long id);

    /**
     * 查询话题关注数量
     *
     * @param pager 分页处理类
     * @return 话题关注数量
     */
    public int queryGroupTopicFollowTotal(Pager pager);

    /**
     * 查询话题关注列表
     * 
     * @param pager 分页处理类
     * @return 话题关注集合
     */
    public List<GroupTopicFollow> selectGroupTopicFollowPager(Pager pager);

    /**
     * 查询需要导出的话题关注列表
     *
     * @param groupTopicFollow 话题关注
     * @return 话题关注集合
     */
    public List<GroupTopicFollow> exportGroupTopicFollowList(GroupTopicFollow groupTopicFollow);
}
