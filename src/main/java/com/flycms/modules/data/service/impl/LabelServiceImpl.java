package com.flycms.modules.data.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.ShortUrlUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.service.ILabelMergeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.data.mapper.LabelMapper;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelService;

import java.util.ArrayList;
import java.util.List;

/**
 * 标签Service业务层处理
 * 
 * @author admin
 * @date 2020-11-18
 */
@Service
public class LabelServiceImpl implements ILabelService 
{
    @Autowired
    private LabelMapper labelMapper;

    @Autowired
    private ILabelMergeService labelMergeService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增标签
     *
     * @param label 标签
     * @return 结果
     */
    @Override
    public int insertLabel(Label label)
    {
        label.setId(SnowFlakeUtils.nextId());
        label.setShortUrl(this.shortUrl());
        label.setStatus("1");
        label.setCreateTime(DateUtils.getNowDate());
        int count = labelMapper.insertLabel(label);
        return count;
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除标签
     *
     * @param ids 需要删除的标签ID
     * @return 结果
     */
    @Override
    public int deleteLabelByIds(Long[] ids)
    {
        return labelMapper.deleteLabelByIds(ids);
    }

    /**
     * 删除标签信息
     *
     * @param id 标签ID
     * @return 结果
     */
    @Override
    public int deleteLabelById(Long id)
    {
        return labelMapper.deleteLabelById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改标签
     *
     * @param label 标签
     * @return 结果
     */
    @Override
    public int updateLabel(Label label)
    {
        return labelMapper.updateLabel(label);
    }

    /**
     * 按id更新标签关联话题统计信息
     *
     * @param id 标签ID
     * @param infoType 信息类别，1话题，2小组
     * @return 结果
     */
    @Override
    public int updateLabelByTopicCount(Long id,int infoType)
    {
        return labelMapper.updateLabelByTopicCount(id,infoType);
    }

    /**
     * 更新标签被关注数量
     *
     * @param id 标签ID
     * @return
     */
    @Override
    public int updateLabelFollowCount(Long id)
    {
        return labelMapper.updateLabelFollowCount(id);
    }

    /**
     * 按id修改标签状态
     *
     * @param id
     * @param status
     * @return
     */
    @Override
    public int updateLabelStatus(Long id,String status)
    {
        return labelMapper.updateLabelStatus(id,status);
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验标签名称是否唯一
     *
     * label 标签实体类
     * @return 结果
     */
     @Override
     public String checkLabelLabelNameUnique(Label label)
     {
         int count = labelMapper.checkLabelLabelNameUnique(label);
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }

    /**
     * 校验短网址是否唯一
     *
     * @param shortUrl 短网址字符串
     * @return 结果
     */
    @Override
    public String checkLabelShorturlUnique(String shortUrl)
    {
        int count = labelMapper.checkLabelShorturlUnique(shortUrl);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询标签短域名是否被占用
     *
     * @return
     */
    @Override
    public String shortUrl(){
        String[] aResult = ShortUrlUtils.shortUrl (null);
        String code=null;
        for ( int i = 0; i < aResult. length ; i++) {
            code=aResult[i];
            //查询问答短域名是否被占用
            if(labelMapper.checkLabelShorturlUnique(code) == 0){
                break;
            }
        }
        return code;
    }

    /**
     * 按shortUrl查询标签信息
     *
     * @param shortUrl
     * @return
     */
    @Override
    public Label findLabelByShorturl(String shortUrl){
        return labelMapper.findLabelByShorturl(shortUrl);
    }

    /**
     * 查询标签
     * 
     * @param id 标签ID
     * @return 标签
     */
    @Override
    @Cacheable(value = "label",key = "'label_'+#id",sync=true)
    public LabelDTO findLabelById(Long id)
    {
        Label label=labelMapper.findLabelById(id);
        if(label != null){
            return BeanConvertor.convertBean(label,LabelDTO.class);
        }
        return null;
    }

    /**
     * title查询标签
     *
     * @param title 标签
     * @return 标签
     */
    @Override
    public Label findLabelByTitle(String title) {
        return labelMapper.findLabelByTitle(title);
    }

    /**
     * 查询标签列表
     *
     * @param label 标签
     * @return 标签
     */
    @Override
    public Pager<LabelDTO> selectLabelPager(Label label, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<LabelDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(label);

        List<Label> labelList=labelMapper.selectLabelPager(pager);
        List<LabelDTO> dtolsit = new ArrayList<LabelDTO>();
        labelList.forEach(entity -> {
            LabelDTO dto = new LabelDTO();
            dto.setId(entity.getId());
            dto.setShortUrl(entity.getShortUrl());
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dto.setLabelPic(entity.getLabelPic());
            dto.setCountTopic(entity.getCountTopic());
            dto.setCountGroup(entity.getCountGroup());
            dto.setCountFollow(entity.getCountFollow());
            dto.setLabelLock(entity.getLabelLock());
            dto.setCreateTime(entity.getCreateTime());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(labelMapper.queryLabelTotal(pager));
        return pager;
    }

    /**
     * 查询随机标签列表
     *
     * @param total 查询数量
     * @return 标签集合
     */
    @Override
    public List<Label> selectRandLabelList(int total) {
        return labelMapper.selectRandLabelList(total);
    }

    /**
     * 查询需要导出的标签列表
     *
     * @param label 标签
     * @return 标签集合
     */
    @Override
    public List<LabelDTO> exportLabelList(Label label) {
        return BeanConvertor.copyList(labelMapper.exportLabelList(label),LabelDTO.class);
    }
}
