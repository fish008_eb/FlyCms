package com.flycms.modules.fullname.controller;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.fullname.domain.Surname;
import com.flycms.modules.fullname.domain.dto.SurnameDTO;
import com.flycms.modules.fullname.service.ISurnameService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 姓氏Controller
 * 
 * @author admin
 * @date 2020-10-13
 */
@RestController
@RequestMapping("/system/fullname/surname")
public class SurnameController extends BaseController
{
    @Autowired
    private ISurnameService surnameService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增姓氏
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:add')")
    @Log(title = "姓氏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Surname surname)
    {
        if (UserConstants.NOT_UNIQUE.equals(surnameService.checkSurnameLastNameUnique(surname)))
        {
            return AjaxResult.error("新增姓氏'" + surname.getLastName() + "'失败，姓氏已存在");
        }
        return toAjax(surnameService.insertSurname(surname));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除姓氏
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:remove')")
    @Log(title = "姓氏", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(surnameService.deleteSurnameByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改姓氏
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:edit')")
    @Log(title = "姓氏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Surname surname)
    {
        return toAjax(surnameService.updateSurname(surname));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询姓氏列表
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:list')")
    @GetMapping("/list")
    public TableDataInfo list(Surname surname,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SurnameDTO> pager = surnameService.selectSurnamePager(surname, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出姓氏列表
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:export')")
    @Log(title = "姓氏", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Surname surname,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SurnameDTO> pager = surnameService.selectSurnamePager(surname, pageNum, pageSize, sort, order);
        ExcelUtil<SurnameDTO> util = new ExcelUtil<SurnameDTO>(SurnameDTO.class);
        return util.exportExcel(pager.getList(), "surname");
    }

    /**
     * 获取姓氏详细信息
     */
    @PreAuthorize("@ss.hasPermi('fullname:surname:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(surnameService.findSurnameById(id));
    }
}
