package com.flycms.modules.fullname.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 姓名搜索数据传输对象 fly_search_keyword
 * 
 * @author admin
 * @date 2020-10-13
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SearchKeywordDTO
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 姓名 */
    @Excel(name = "姓名")
    private String fullName;
    /** 搜索次数 */
    @Excel(name = "搜索次数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countView;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
