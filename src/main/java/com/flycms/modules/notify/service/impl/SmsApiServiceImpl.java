package com.flycms.modules.notify.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsApi;
import com.flycms.modules.notify.domain.dto.SmsApiDto;
import com.flycms.modules.notify.domain.po.SmsApiUnionSignPO;
import com.flycms.modules.notify.mapper.SmsApiMapper;
import com.flycms.modules.notify.service.ISmsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 短信接口Service业务层处理
 * 
 * @author admin
 * @date 2020-05-27
 */
@Service
public class SmsApiServiceImpl implements ISmsApiService
{
    @Autowired
    private SmsApiMapper smsApiMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    @Override
    public int insertSmsApi(SmsApi smsApi)
    {
        smsApi.setId(SnowFlakeUtils.nextId());
        smsApi.setCreateTime(DateUtils.getNowDate());
        return smsApiMapper.insertSmsApi(smsApi);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信接口
     *
     * @param ids 需要删除的短信接口ID
     * @return 结果
     */
    @Override
    public int deleteSmsApiByIds(Long[] ids)
    {
        return smsApiMapper.deleteSmsApiByIds(ids);
    }

    /**
     * 删除短信接口信息
     *
     * @param id 短信接口ID
     * @return 结果
     */
    @Override
    public int deleteSmsApiById(Long id)
    {
        return smsApiMapper.deleteSmsApiById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    @Override
    public int updateSmsApi(SmsApi smsApi)
    {
        return smsApiMapper.updateSmsApi(smsApi);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信接口
     * 
     * @param id 短信接口ID
     * @return 短信接口
     */
    @Override
    public SmsApi selectSmsApiById(Long id)
    {
        return smsApiMapper.selectSmsApiById(id);
    }

    /**
     * 按模板id查询手机短信模板信息
     *
     * @param tpId
     * @return
     */
    public SmsApiUnionSignPO findSmsApiUnionSignByTpId(Long tpId)
    {
        return smsApiMapper.findSmsApiUnionSignByTpId(tpId);
    }

    /**
     * 查询短信接口列表
     *
     * @param smsApi 短信接口
     * @return 短信接口
     */
    @Override
    public Pager<SmsApiDto> selectSmsApiPager(SmsApi smsApi, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SmsApiDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(smsApi);

        List<SmsApi> smsApiList=smsApiMapper.selectSmsApiPager(pager);
        List<SmsApiDto> dtolsit = new ArrayList<SmsApiDto>();
        smsApiList.forEach(smsApis -> {
            SmsApiDto smsApiDto = new SmsApiDto();
            smsApiDto.setId(smsApis.getId());
            smsApiDto.setApiName(smsApis.getApiName());
            smsApiDto.setAccessKeyId(smsApis.getAccessKeyId());
            smsApiDto.setApiUrl(smsApis.getApiUrl());
            smsApiDto.setStatus(smsApis.getStatus());
            dtolsit.add(smsApiDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(smsApiMapper.querySmsApiTotal(pager));
        return pager;
    }

}
