package com.flycms.modules.notify.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsTemplate;
import com.flycms.modules.notify.mapper.SmsTemplateMapper;
import com.flycms.modules.notify.service.ISmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.notify.domain.dto.SmsTemplateDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 短信模板Service业务层处理
 * 
 * @author admin
 * @date 2020-05-27
 */
@Service
public class SmsTemplateServiceImpl implements ISmsTemplateService
{
    @Autowired
    private SmsTemplateMapper smsTemplateMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    @Override
    public int insertSmsTemplate(SmsTemplate smsTemplate)
    {
        smsTemplate.setId(SnowFlakeUtils.nextId());
        smsTemplate.setCreateTime(DateUtils.getNowDate());
        return smsTemplateMapper.insertSmsTemplate(smsTemplate);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信模板
     *
     * @param ids 需要删除的短信模板ID
     * @return 结果
     */
    @Override
    public int deleteSmsTemplateByIds(Long[] ids)
    {
        return smsTemplateMapper.deleteSmsTemplateByIds(ids);
    }

    /**
     * 删除短信模板信息
     *
     * @param id 短信模板ID
     * @return 结果
     */
    @Override
    public int deleteSmsTemplateById(Long id)
    {
        return smsTemplateMapper.deleteSmsTemplateById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    @Override
    public int updateSmsTemplate(SmsTemplate smsTemplate)
    {
        return smsTemplateMapper.updateSmsTemplate(smsTemplate);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验签名id是否唯一
     *
     * @param signId 短信模板
     * @return 结果
     */
     @Override
     public String checkSmsTemplateSignIdUnique(Long signId)
     {
         int count = smsTemplateMapper.checkSmsTemplateSignIdUnique(signId);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 校验模板名称是否唯一
     *
     * @param templateName 短信模板
     * @return 结果
     */
     @Override
     public String checkSmsTemplateTemplateNameUnique(String templateName)
     {
         int count = smsTemplateMapper.checkSmsTemplateTemplateNameUnique(templateName);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询短信模板
     * 
     * @param id 短信模板ID
     * @return 短信模板
     */
    @Override
    public SmsTemplate selectSmsTemplateById(Long id)
    {
        return smsTemplateMapper.selectSmsTemplateById(id);
    }


    /**
     * 查询短信模板列表
     *
     * @param smsTemplate 短信模板
     * @return 短信模板
     */
    @Override
    public Pager<SmsTemplateDto> selectSmsTemplatePager(SmsTemplate smsTemplate, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SmsTemplateDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(smsTemplate);

        List<SmsTemplate> smsTemplateList=smsTemplateMapper.selectSmsTemplatePager(pager);
        List<SmsTemplateDto> dtolsit = new ArrayList<SmsTemplateDto>();
        smsTemplateList.forEach(smsTemplates -> {
            SmsTemplateDto smsTemplateDto = new SmsTemplateDto();
            smsTemplateDto.setId(smsTemplates.getId());
            smsTemplateDto.setSignId(smsTemplates.getSignId());
            smsTemplateDto.setTemplateName(smsTemplates.getTemplateName());
            smsTemplateDto.setTemplateCode(smsTemplates.getTemplateCode());
            smsTemplateDto.setDetail(smsTemplates.getDetail());
            smsTemplateDto.setStatus(smsTemplates.getStatus());
            dtolsit.add(smsTemplateDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(smsTemplateMapper.querySmsTemplateTotal(pager));
        return pager;
    }

}
