package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsCode;
import com.flycms.modules.notify.domain.dto.SmsCodeDto;

/**
 * 短信验证码Service接口
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
public interface ISmsCodeService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    public int insertSmsCode(SmsCode smsCode);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信验证码
     *
     * @param ids 需要删除的短信验证码ID
     * @return 结果
     */
    public int deleteSmsCodeByIds(Long[] ids);

    /**
     * 删除短信验证码信息
     *
     * @param id 短信验证码ID
     * @return 结果
     */
    public int deleteSmsCodeById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    public int updateSmsCode(SmsCode smsCode);

    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param infoType  信息类型，0手机，1邮箱
     * @param userName  按用户名（邮箱、手机号）
     * @param code  验证码
     * @return
     */
    public int updateSmsCodeByStatus(int infoType,String userName,String code);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询指定日期内申请验证码次数
     *
     * @param userName
     * @param createTime
     * @return
     */
    public int checkSmsCodeCount(String userName, String createTime);

    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userName
     *         查询的用户名
     * @param codeType
     *         查询的验证码类型，1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @param code
     *         验证码
     * @return
     */
    public boolean checkSmsCodeCode(String userName,Integer codeType,String code);

    /**
     * 查询短信验证码
     * 
     * @param id 短信验证码ID
     * @return 短信验证码
     */
    public SmsCode selectSmsCodeById(Long id);

    /**
     * 查询短信验证码列表
     * 
     * @param smsCode 短信验证码
     * @return 短信验证码集合
     */
    public Pager<SmsCodeDto> selectSmsCodePager(SmsCode smsCode, Integer page, Integer limit, String sort, String order);
}
