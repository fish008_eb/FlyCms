package com.flycms.modules.notify.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信模板对象 fly_sms_template
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
public class SmsTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 签名id */
    private Long signId;
    /** 模板名称 */
    private String templateName;
    /** 模板代码 */
    private String templateCode;
    /** 模板描述 */
    private String detail;
    /** 状态 */
    private Integer status;
}
