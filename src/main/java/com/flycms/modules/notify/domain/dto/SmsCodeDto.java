package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 短信验证码数据传输对象 fly_sms_code
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SmsCodeDto
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 信息类型 */
    @Excel(name = "信息类型")
    private Integer infoType;
    /** 用户id */
    @Excel(name = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;
    /** 验证码 */
    @Excel(name = "验证码")
    private String code;
    /** 验证码类型 */
    @Excel(name = "验证码类型")
    private Integer codeType;
    /** 激活状态，0未激活，1已激活 */
    @Excel(name = "激活状态")
    private Integer referStatus;
    @Excel(name = "添加时间")
    private Long createTime;
}
