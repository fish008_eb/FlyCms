package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信模板数据传输对象 fly_sms_template
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SmsTemplateDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 签名id */
    @Excel(name = "签名id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long signId;
    /** 模板名称 */
    @Excel(name = "模板名称")
    private String templateName;
    /** 模板代码 */
    @Excel(name = "模板代码")
    private String templateCode;
    /** 模板描述 */
    @Excel(name = "模板描述")
    private String detail;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
}
