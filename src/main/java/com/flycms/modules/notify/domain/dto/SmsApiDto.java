package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信接口数据传输对象 fly_sms_api
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SmsApiDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 接口名称 */
    @Excel(name = "接口名称")
    private String apiName;
    /** KeyId */
    @Excel(name = "KeyId")
    private String accessKeyId;
    /** 接口网址 */
    @Excel(name = "接口网址")
    private String apiUrl;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
}
