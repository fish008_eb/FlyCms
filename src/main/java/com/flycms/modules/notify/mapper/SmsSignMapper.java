package com.flycms.modules.notify.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsSign;
import org.springframework.stereotype.Repository;

/**
 * 短信签名Mapper接口
 * 
 * @author admin
 * @date 2020-05-27
 */
@Repository
public interface SmsSignMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    public int insertSmsSign(SmsSign smsSign);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信签名
     *
     * @param id 短信签名ID
     * @return 结果
     */
    public int deleteSmsSignById(Long id);

    /**
     * 批量删除短信签名
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsSignByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    public int updateSmsSign(SmsSign smsSign);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信签名
     * 
     * @param id 短信签名ID
     * @return 短信签名
     */
    public SmsSign selectSmsSignById(Long id);

    /**
     * 查询短信签名数量
     *
     * @param pager 分页处理类
     * @return 短信签名数量
     */
    public int querySmsSignTotal(Pager pager);

    /**
     * 查询短信签名列表
     * 
     * @param pager 分页处理类
     * @return 短信签名集合
     */
    public List<SmsSign> selectSmsSignPager(Pager pager);

}
