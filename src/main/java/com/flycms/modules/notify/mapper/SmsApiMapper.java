package com.flycms.modules.notify.mapper;

import java.util.List;

import com.flycms.modules.notify.domain.SmsApi;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.po.SmsApiUnionSignPO;
import org.springframework.stereotype.Repository;

/**
 * 短信接口Mapper接口
 * 
 * @author admin
 * @date 2020-05-27
 */
@Repository
public interface SmsApiMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    public int insertSmsApi(SmsApi smsApi);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信接口
     *
     * @param id 短信接口ID
     * @return 结果
     */
    public int deleteSmsApiById(Long id);

    /**
     * 批量删除短信接口
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsApiByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信接口
     *
     * @param smsApi 短信接口
     * @return 结果
     */
    public int updateSmsApi(SmsApi smsApi);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信接口
     * 
     * @param id 短信接口ID
     * @return 短信接口
     */
    public SmsApi selectSmsApiById(Long id);

    /**
     * 按模板id查询手机短信模板信息
     *
     * @param tpId
     * @return
     */
    public SmsApiUnionSignPO findSmsApiUnionSignByTpId(Long tpId);

    /**
     * 查询短信接口数量
     *
     * @param pager 分页处理类
     * @return 短信接口数量
     */
    public int querySmsApiTotal(Pager pager);

    /**
     * 查询短信接口列表
     * 
     * @param pager 分页处理类
     * @return 短信接口集合
     */
    public List<SmsApi> selectSmsApiPager(Pager pager);

}
