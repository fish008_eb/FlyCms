package com.flycms.modules.notify.controller.manage;

import com.flycms.modules.notify.service.ISmsApiService;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.SmsApi;
import com.flycms.modules.notify.domain.dto.SmsApiDto;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 短信接口Controller
 * 
 * @author admin
 * @date 2020-05-27
 */
@RestController
@RequestMapping("/system/notify/SmsApi")
public class SmsApiAdminController extends BaseController
{
    @Autowired
    private ISmsApiService smsApiService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增短信接口
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:add')")
    @Log(title = "短信接口", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsApi smsApi)
    {
        return toAjax(smsApiService.insertSmsApi(smsApi));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信接口
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:remove')")
    @Log(title = "短信接口", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsApiService.deleteSmsApiByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信接口
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:edit')")
    @Log(title = "短信接口", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsApi smsApi)
    {
        return toAjax(smsApiService.updateSmsApi(smsApi));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询短信接口列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsApi smsApi,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsApiDto> pager = smsApiService.selectSmsApiPager(smsApi, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出短信接口列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:export')")
    @Log(title = "短信接口", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsApi smsApi,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsApiDto> pager = smsApiService.selectSmsApiPager(smsApi, pageNum, pageSize, sort, order);
        ExcelUtil<SmsApiDto> util = new ExcelUtil<SmsApiDto>(SmsApiDto.class);
        return util.exportExcel(pager.getList(), "SmsApi");
    }

    /**
     * 获取短信接口详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsApi:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsApiService.selectSmsApiById(id));
    }
}
