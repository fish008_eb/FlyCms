package com.flycms.modules.user.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.domain.dto.UserFansDTO;
import com.flycms.modules.user.service.IUserFansService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 用户分析查询关联表Controller
 * 
 * @author admin
 * @date 2020-11-27
 */
@RestController
@RequestMapping("/system/user/fans")
public class UserFansAdminController extends BaseController
{
    @Autowired
    private IUserFansService userFansService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增用户分析查询关联表
     */
    @PreAuthorize("@ss.hasPermi('user:fans:add')")
    @Log(title = "用户分析查询关联表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserFans userFans)
    {
        return toAjax(userFansService.insertUserFans(userFans));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户分析查询关联表
     */
    @PreAuthorize("@ss.hasPermi('user:fans:remove')")
    @Log(title = "用户分析查询关联表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(userFansService.deleteUserFansByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户分析查询关联表
     */
    @PreAuthorize("@ss.hasPermi('user:fans:edit')")
    @Log(title = "用户分析查询关联表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserFans userFans)
    {
        return toAjax(userFansService.updateUserFans(userFans));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询用户分析查询关联表列表
     */
    @PreAuthorize("@ss.hasPermi('user:fans:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserFans userFans,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<UserFansDTO> pager = userFansService.selectUserFansPager(userFans, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出用户分析查询关联表列表
     */
    @PreAuthorize("@ss.hasPermi('user:fans:export')")
    @Log(title = "用户分析查询关联表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserFans userFans)
    {
        List<UserFansDTO> userFansList = userFansService.exportUserFansList(userFans);
        ExcelUtil<UserFansDTO> util = new ExcelUtil<UserFansDTO>(UserFansDTO.class);
        return util.exportExcel(userFansList, "fans");
    }

    /**
     * 获取用户分析查询关联表详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:fans:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(userFansService.findUserFansById(id));
    }

}
