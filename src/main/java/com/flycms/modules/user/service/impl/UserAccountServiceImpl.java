package com.flycms.modules.user.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.user.mapper.UserAccountMapper;
import com.flycms.modules.user.domain.UserAccount;
import com.flycms.modules.user.domain.dto.UserAccountDTO;
import com.flycms.modules.user.service.IUserAccountService;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户账户Service业务层处理
 * 
 * @author admin
 * @date 2020-12-04
 */
@Service
public class UserAccountServiceImpl implements IUserAccountService 
{
    @Autowired
    private UserAccountMapper userAccountMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    @Override
    public int insertUserAccount(UserAccount userAccount)
    {
        return userAccountMapper.insertUserAccount(userAccount);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户账户
     *
     * @param userIds 需要删除的用户账户ID
     * @return 结果
     */
    @Override
    public int deleteUserAccountByIds(Long[] userIds)
    {
        return userAccountMapper.deleteUserAccountByIds(userIds);
    }

    /**
     * 删除用户账户信息
     *
     * @param userId 用户账户ID
     * @return 结果
     */
    @Override
    public int deleteUserAccountById(Long userId)
    {
        return userAccountMapper.deleteUserAccountById(userId);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    @Override
    public int updateUserAccount(UserAccount userAccount)
    {
        return userAccountMapper.updateUserAccount(userAccount);
    }

    /**
     * 更新用户积分
     *
     * @param calculate
     *        运算：plus是加+,reduce是减-
     * @param score
     *        积分数量
     * @param userId
     *        用户id
     * @return
     */
    @Override
    public int updateUserAccountScore(String calculate, Integer score, Long userId){
        UserAccount userAccount=userAccountMapper.findUserAccountById(userId);
        if(userAccount != null && score > 0){
            UserAccount account = new UserAccount();
            account.setUserId(userId);
            if("plus".equals(calculate)){
                userAccount.setScore(userAccount.getScore()+score);
            }else if("reduce".equals(calculate)){
                userAccount.setScore(userAccount.getScore()-score);
            }
            userAccountMapper.updateUserAccount(account);
        }
        return 0;
    }

    /**
     * 更新用户发布话题数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserTopic(Long userId)
    {
        return userAccountMapper.updateUserTopic(userId);
    }

    /**
     * 更新用户加入的小组数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserGroup(Long userId)
    {
        return userAccountMapper.updateUserGroup(userId);
    }

    /**
     * 更新用户所有粉丝数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserFans(Long userId)
    {
        return userAccountMapper.updateUserFans(userId);
    }

    /**
     * 更新用户被关注数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserFollow(Long userId)
    {
        return userAccountMapper.updateUserFollow(userId);
    }

    /**
     * 更新用户关注话题数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserFollowTopic(Long userId)
    {
        return userAccountMapper.updateUserFollowTopic(userId);
    }


    /**
     * 更新关注标签数量
     *
     * @param userId 用户id
     * @return 结果
     */
    @Override
    public int updateUserFollowLabel(Long userId)
    {
        return userAccountMapper.updateUserFollowLabel(userId);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验用户id是否唯一
     *
     * @param userId 用户id
     * @param userId 用户id
     * @return 结果
     */
     @Override
     public String checkUserAccountUserIdUnique(Long userId)
     {
         UserAccount userAccount = new UserAccount();
         userAccount.setUserId(userId);
         int count = userAccountMapper.checkUserAccountUserIdUnique(userAccount);
         if (count > 0){
             return UserConstants.NOT_UNIQUE;
         }
         return UserConstants.UNIQUE;
     }


    /**
     * 查询用户账户
     * 
     * @param userId 用户账户ID
     * @return 用户账户
     */
    @Override
    public UserAccountDTO findUserAccountById(Long userId)
    {
        UserAccount userAccount=userAccountMapper.findUserAccountById(userId);
        return BeanConvertor.convertBean(userAccount,UserAccountDTO.class);
    }


    /**
     * 查询用户账户列表
     *
     * @param userAccount 用户账户
     * @return 用户账户
     */
    @Override
    public Pager<UserAccountDTO> selectUserAccountPager(UserAccount userAccount, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<UserAccountDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(userAccount);

        List<UserAccount> userAccountList=userAccountMapper.selectUserAccountPager(pager);
        List<UserAccountDTO> dtolsit = new ArrayList<UserAccountDTO>();
        userAccountList.forEach(entity -> {
            UserAccountDTO dto = new UserAccountDTO();
            dto.setUserId(entity.getUserId());
            dto.setBalance(entity.getBalance());
            dto.setScore(entity.getScore());
            dto.setExp(entity.getExp());
            dto.setCountTopic(entity.getCountTopic());
            dto.setCountGroup(entity.getCountGroup());
            dto.setCountFans(entity.getCountFans());
            dto.setCountFollow(entity.getCountFollow());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userAccountMapper.queryUserAccountTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的用户账户列表
     *
     * @param userAccount 用户账户
     * @return 用户账户集合
     */
    @Override
    public List<UserAccountDTO> exportUserAccountList(UserAccount userAccount) {
        return BeanConvertor.copyList(userAccountMapper.exportUserAccountList(userAccount),UserAccountDTO.class);
    }
}
