package com.flycms.modules.user.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserAppMerge;
import com.flycms.modules.user.mapper.UserAppMergeMapper;
import com.flycms.modules.user.service.IUserAppMergeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.user.domain.dto.UserAppMergeDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户和app关联Service业务层处理
 * 
 * @author admin
 * @date 2020-05-31
 */
@Service
public class UserAppMergeServiceImpl implements IUserAppMergeService
{
    @Autowired
    private UserAppMergeMapper userAppMergeMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    @Override
    public int insertUserAppMerge(UserAppMerge userAppMerge)
    {
        return userAppMergeMapper.insertUserAppMerge(userAppMerge);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户和app关联
     *
     * @param appIds 需要删除的用户和app关联ID
     * @return 结果
     */
    @Override
    public int deleteUserAppMergeByIds(Long[] appIds)
    {
        return userAppMergeMapper.deleteUserAppMergeByIds(appIds);
    }

    /**
     * 删除用户和app关联信息
     *
     * @param appId 用户和app关联ID
     * @return 结果
     */
    @Override
    public int deleteUserAppMergeById(Long appId)
    {
        return userAppMergeMapper.deleteUserAppMergeById(appId);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    @Override
    public int updateUserAppMerge(UserAppMerge userAppMerge)
    {
        return userAppMergeMapper.updateUserAppMerge(userAppMerge);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验APPID或者用户id是否唯一
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
     @Override
     public String checkUserAppMergeUnique(UserAppMerge userAppMerge)
     {
         int count = userAppMergeMapper.checkUserAppMergeUnique(userAppMerge);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 查询用户和app关联
     * 
     * @param appId 用户和app关联ID
     * @return 用户和app关联
     */
    @Override
    public UserAppMerge selectUserAppMergeById(Long appId)
    {
        return userAppMergeMapper.selectUserAppMergeById(appId);
    }


    /**
     * 查询用户和app关联列表
     *
     * @param userAppMerge 用户和app关联
     * @return 用户和app关联
     */
    @Override
    public Pager<UserAppMergeDto> selectUserAppMergePager(UserAppMerge userAppMerge, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<UserAppMergeDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(userAppMerge);

        List<UserAppMerge> userAppMergeList=userAppMergeMapper.selectUserAppMergePager(pager);
        List<UserAppMergeDto> dtolsit = new ArrayList<UserAppMergeDto>();
        userAppMergeList.forEach(userAppMerges -> {
            UserAppMergeDto userAppMergeDto = new UserAppMergeDto();
            userAppMergeDto.setAppId(userAppMerges.getAppId());
            userAppMergeDto.setUserId(userAppMerges.getUserId());
            dtolsit.add(userAppMergeDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userAppMergeMapper.queryUserAppMergeTotal(pager));
        return pager;
    }

}
