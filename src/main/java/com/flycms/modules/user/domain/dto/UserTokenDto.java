package com.flycms.modules.user.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.time.LocalDateTime;
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class UserTokenDto {
	@JsonSerialize(using = ToStringSerializer.class)
	private Integer userId;
	private String token;
	private String sessionKey;
	private LocalDateTime expireTime;
	private LocalDateTime updateTime;
}
