package com.flycms.modules.user.domain.vo;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

@Data
public class RegUserVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 邮箱 */
    private String email;
    /** 密码 */
    private String password;
    private String password2;
}
