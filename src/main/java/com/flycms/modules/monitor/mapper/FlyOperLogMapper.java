package com.flycms.modules.monitor.mapper;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.OperLog;
import org.springframework.stereotype.Repository;

/**
 * 操作日志 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyOperLogMapper
{
    /**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
    public void insertOperlog(OperLog operLog);


    /**
     * 批量删除系统操作日志
     * 
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public int deleteOperLogByIds(Long[] operIds);

    /**
     * 查询操作日志详细
     * 
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public OperLog selectOperLogById(Long operId);

    /**
     * 清空操作日志
     */
    public void cleanOperLog();


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询系统操作日志总数
     *
     * @param pager
     * @return
     */
    public int queryOperLogTotal(Pager pager);

    /**
     * 查询系统操作日志集合
     *
     * @param pager 操作日志对象
     * @return 操作日志集合
     */
    public List<OperLog> selectOperLogPager(Pager pager);

    /**
     * 查询需要导出的操作日志记录列表
     *
     * @param operLog 操作日志记录
     * @return 操作日志记录集合
     */
    public List<OperLog> exportOperLogList(OperLog operLog);
}
