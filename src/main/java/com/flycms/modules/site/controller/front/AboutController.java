package com.flycms.modules.site.controller.front;

import com.flycms.common.constant.Constants;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.file.FileUploadUtils;
import com.flycms.common.utils.file.FileUtils;
import com.flycms.framework.config.ServerConfig;
import com.flycms.framework.config.properties.FlyMallProperties;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.group.domain.dto.GroupDTO;
import com.flycms.modules.site.domain.dto.SiteAboutDTO;
import com.flycms.modules.site.service.ISiteAboutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 关于我们Controller
 * 
 * @author kaifei sun
 */
@Controller
public class AboutController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(AboutController.class);

    @Autowired
    private ISiteAboutService siteAboutService;

    /**
     * 关于我们详情页面
     *
     * @return
     */
    @GetMapping("/about/{id}")
    public String about(@PathVariable(value = "id", required = false) String id,ModelMap modelMap){
        if (!StrUtils.checkLong(id)) {
            return forward("error/404");
        }
        SiteAboutDTO content=siteAboutService.findSiteAboutById(Long.valueOf(id));
        modelMap.addAttribute("content", content);
        return theme.getPcTemplate("about/detail");
    }

    /**
     * 关于我们详情页面
     *
     * @return
     */
    @ResponseBody
    @GetMapping("/about/agreement")
    public AjaxResult agreement(ModelMap modelMap){
        return AjaxResult.success(siteAboutService.findSiteAboutById(551796453056446464l));
    }
}
