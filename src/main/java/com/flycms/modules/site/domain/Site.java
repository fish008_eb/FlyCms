package com.flycms.modules.site.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 官网设置对象 fly_site
 * 
 * @author admin
 * @date 2020-07-08
 */
@Data
public class Site extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;
    /** 网站名称 */
    private String siteName;

    private String siteUrl;
    /** Banner广告 */
    private Integer showBanner;
    /** pc模板名称 */
    private String pcTemplate;
    /** 移动端模板名称 */
    private String mobileTemplate;
    /** PC端的LOGO */
    private String logo;
    /** 验证码过期时间，以分钟为单位 */
    private int codeExpiresTime;
    /** 用户注册是否审核，0不审核，1审核 */
    private int userVerify;
    /** 用户发帖审核，0不审核，1审核 */
    private int postVerify;
    /** 用户发评论，0不审核，1审核 */
    private int commentVerify;
    /** 网站标题 */
    private String title;
    /** 搜索的关键词 */
    private String keywords;
    /** 网站描述 */
    private String description;
    /** 违禁关键词设置，英文逗号隔开 */
    private String sensitiveWord;
    /** 网站版权设置 */
    private String copyright;
    /** 注册验证码 */
    private Long smsRegister;
    /** 找回密码短信 */
    private Long smsFindPassword;
    /** 更换手机号码 */
    private Long smsReplacePhone;
    /** 站点状态 */
    private Integer status;
}
