package com.flycms.modules.system.controller;

import java.io.IOException;

import com.flycms.common.utils.SecurityUtils;
import com.flycms.common.utils.ServletUtils;
import com.flycms.common.utils.file.FileUploadUtils;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.config.properties.FlyMallProperties;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.framework.security.service.TokenService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.system.domain.FlyAdmin;
import com.flycms.modules.system.service.IFlyAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 个人信息 业务处理
 *
 * @author kaifei sun
 */
@RestController
@RequestMapping("/system/admin/profile")
public class FlyProfileController extends BaseController
{
    @Autowired
    private IFlyAdminService userService;

    @Autowired
    private TokenService tokenService;

    /**
     * 个人信息
     */
    @GetMapping
    public AjaxResult profile()
    {
        LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
        FlyAdmin user = loginAdmin.getAdmin();
        AjaxResult ajax = AjaxResult.success(user);
        ajax.put("roleGroup", userService.selectAdminRoleGroup(loginAdmin.getUsername()));
        ajax.put("postGroup", userService.selectAdminPostGroup(loginAdmin.getUsername()));
        return ajax;
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult updateProfile(@RequestBody FlyAdmin admin)
    {
        if (userService.updateAdminProfile(admin) > 0)
        {
            LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
            // 更新缓存用户信息
            loginAdmin.getAdmin().setNickName(admin.getNickName());
            loginAdmin.getAdmin().setPhonenumber(admin.getPhonenumber());
            loginAdmin.getAdmin().setEmail(admin.getEmail());
            loginAdmin.getAdmin().setSex(admin.getSex());
            tokenService.setLoginAdmin(loginAdmin);
            return AjaxResult.success();
        }
        return AjaxResult.error("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public AjaxResult updatePwd(String oldPassword, String newPassword)
    {
        LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
        String userName = loginAdmin.getUsername();
        String password = loginAdmin.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password))
        {
            return AjaxResult.error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password))
        {
            return AjaxResult.error("新密码不能与旧密码相同");
        }
        if (userService.resetAdminPwd(userName, SecurityUtils.encryptPassword(newPassword)) > 0)
        {
            // 更新缓存用户密码
            loginAdmin.getAdmin().setPassword(SecurityUtils.encryptPassword(newPassword));
            tokenService.setLoginAdmin(loginAdmin);
            return AjaxResult.success();
        }
        return AjaxResult.error("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @PostMapping("/avatar")
    public AjaxResult avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException
    {
        if (!file.isEmpty())
        {
            LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
            String avatar = FileUploadUtils.upload(FlyMallProperties.getAvatarPath(), file);
            if (userService.updateAdminAvatar(loginAdmin.getUsername(), avatar))
            {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar);
                // 更新缓存用户头像
                loginAdmin.getAdmin().setAvatar(avatar);
                tokenService.setLoginAdmin(loginAdmin);
                return ajax;
            }
        }
        return AjaxResult.error("上传图片异常，请联系管理员");
    }
}
