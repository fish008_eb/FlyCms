package com.flycms.modules.system.mapper;

import java.util.List;
import com.flycms.modules.system.domain.FlyConfig;
import org.springframework.stereotype.Repository;

/**
 * 参数配置 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyConfigMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    public int insertConfig(FlyConfig config);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除参数配置
     *
     * @param configId 参数ID
     * @return 结果
     */
    public int deleteConfigById(Long configId);

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     * @return 结果
     */
    public int deleteConfigByIds(Long[] configIds);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfig(FlyConfig config);
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询参数配置信息
     * 
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public FlyConfig selectConfig(FlyConfig config);

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<FlyConfig> selectConfigList(FlyConfig config);

    /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数键名
     * @return 参数配置信息
     */
    public FlyConfig checkConfigKeyUnique(String configKey);
}