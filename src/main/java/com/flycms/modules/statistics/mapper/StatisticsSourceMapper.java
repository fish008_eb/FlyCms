package com.flycms.modules.statistics.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsSource;
import org.springframework.stereotype.Repository;

/**
 * 来源统计Mapper接口
 * 
 * @author admin
 * @date 2020-12-09
 */
@Repository
public interface StatisticsSourceMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    public int insertStatisticsSource(StatisticsSource statisticsSource);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除来源统计
     *
     * @param id 来源统计ID
     * @return 结果
     */
    public int deleteStatisticsSourceById(Long id);

    /**
     * 批量删除来源统计
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStatisticsSourceByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改来源统计
     *
     * @param statisticsSource 来源统计
     * @return 结果
     */
    public int updateStatisticsSource(StatisticsSource statisticsSource);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询来源统计
     * 
     * @param id 来源统计ID
     * @return 来源统计
     */
    public StatisticsSource findStatisticsSourceById(Long id);

    /**
     * 查询来源统计数量
     *
     * @param pager 分页处理类
     * @return 来源统计数量
     */
    public int queryStatisticsSourceTotal(Pager pager);

    /**
     * 查询来源统计列表
     * 
     * @param pager 分页处理类
     * @return 来源统计集合
     */
    public List<StatisticsSource> selectStatisticsSourcePager(Pager pager);

    /**
     * 查询需要导出的来源统计列表
     *
     * @param statisticsSource 来源统计
     * @return 来源统计集合
     */
    public List<StatisticsSource> exportStatisticsSourceList(StatisticsSource statisticsSource);
}
