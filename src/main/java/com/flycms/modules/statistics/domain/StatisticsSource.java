package com.flycms.modules.statistics.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 来源统计对象 fly_statistics_source
 * 
 * @author admin
 * @date 2020-12-09
 */
@Data
public class StatisticsSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 统计日期（格式:yyyy-MM-dd） */
    private String statisticsDay;
    /** 来源网站类型 （1-搜索引擎  2-外部链接  3-直接访问） */
    private Integer sorceUrlType;
    /** 是否新客户 （0-否   1-是） */
    private Integer isNewVisitor;
    /** 访客设备类型（1-计算机   2-移动设备） */
    private Integer visitorDeviceType;
    /** 来源域名 */
    private String sourceDomain;
    /** 来源外部链接网站地址或网站名称（如：百度/http://www.jeecms.com） */
    private String sorceUrl;
    /** 搜索引擎 */
    private String engineName;
    /** 浏览量 */
    private Long pvs;
    /** 访客数 */
    private Long uvs;
    /** ip数 */
    private Long ips;
    /** 总访问时长(单位：秒) */
    private Long accessHoureLong;
    /** 只访问一次页面的访问次数 */
    private Long onlyOnePv;
    /** 时间段 */
    private Integer statisticsHour;
    /** 删除标识 */
    private Integer deleted;
}
