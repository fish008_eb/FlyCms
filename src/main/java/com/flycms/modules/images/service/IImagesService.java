package com.flycms.modules.images.service;

public interface IImagesService {


    /**
     * 保存内容中的图片本地化路径处理
     *
     * @param typeId
     *         信息类型，0问题，1答案，2文章，3分享
     * @param infoId
     *         信息id
     * @param userId
     *         用户id
     * @param content
     *         需要分析处理并下载的内容
     * @return
     * @throws Exception
     */
    public String replaceContent(Integer typeId,Long infoId,Long userId,String content);
}
