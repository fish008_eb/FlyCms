package com.flycms.modules.score.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 积分日志数据传输对象 fly_score_detail
 *
 * @author admin
 * @date 2020-12-01
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ScoreDetailDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 类型 */
    @Excel(name = "类型")
    private String type;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 积分 */
    @Excel(name = "积分")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer score;
    /** 余额 */
    @Excel(name = "余额")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer balance;
    /** 内容ID */
    @Excel(name = "内容ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long foreignId;
    /** 规则ID */
    @Excel(name = "规则ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long scoreRuleId;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer status;
    /** 创建时间 */
    @Excel(name = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Long createTime;
}
